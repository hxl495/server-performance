package com.hxl.handlers;


import com.alibaba.fastjson.JSON;
import com.hxl.os.linux.Performance;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

public class PerformanceHandler implements HttpHandler {
    private Performance performance =new Performance();
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String result = JSON.toJSONString(performance.getInfos());
        httpExchange.sendResponseHeaders(200, result.length());
        OutputStream os = httpExchange.getResponseBody();
        os.write(result.getBytes());
        os.close();
    }
}
