package com.hxl.os.linux;
import com.hxl.os.ComputerInfos;
import com.hxl.os.CpuInfos;
import com.hxl.os.MemoryInfos;
import com.hxl.os.SysInfos;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.GlobalMemory;

import java.text.DecimalFormat;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Performance {

    private CpuInfos getCpuInfo() {
        try {
            SystemInfo systemInfo = new SystemInfo();
            CentralProcessor processor = systemInfo.getHardware().getProcessor();
            long[] prevTicks = processor.getSystemCpuLoadTicks();
            TimeUnit.SECONDS.sleep(1);
            long[] ticks = processor.getSystemCpuLoadTicks();
            long nice = ticks[CentralProcessor.TickType.NICE.getIndex()] - prevTicks[CentralProcessor.TickType.NICE.getIndex()];
            long irq = ticks[CentralProcessor.TickType.IRQ.getIndex()] - prevTicks[CentralProcessor.TickType.IRQ.getIndex()];
            long softirq = ticks[CentralProcessor.TickType.SOFTIRQ.getIndex()] - prevTicks[CentralProcessor.TickType.SOFTIRQ.getIndex()];
            long steal = ticks[CentralProcessor.TickType.STEAL.getIndex()] - prevTicks[CentralProcessor.TickType.STEAL.getIndex()];
            long cSys = ticks[CentralProcessor.TickType.SYSTEM.getIndex()] - prevTicks[CentralProcessor.TickType.SYSTEM.getIndex()];
            long user = ticks[CentralProcessor.TickType.USER.getIndex()] - prevTicks[CentralProcessor.TickType.USER.getIndex()];
            long iowait = ticks[CentralProcessor.TickType.IOWAIT.getIndex()] - prevTicks[CentralProcessor.TickType.IOWAIT.getIndex()];
            long idle = ticks[CentralProcessor.TickType.IDLE.getIndex()] - prevTicks[CentralProcessor.TickType.IDLE.getIndex()];
            long totalCpu = user + nice + cSys + idle + iowait + irq + softirq + steal;
            return  new CpuInfos(processor.getLogicalProcessorCount(),(float) (1.0-(idle * 1.0 / totalCpu)));
        }catch (InterruptedException e){}
        return null;
    }

    private  MemoryInfos getMemInfo(){
        SystemInfo systemInfo = new SystemInfo();
        GlobalMemory memory = systemInfo.getHardware().getMemory();
        //
        long totalByte = memory.getTotal();
        long acaliableByte = memory.getAvailable();

        return  new MemoryInfos(totalByte,acaliableByte,(totalByte-acaliableByte)*1.0/totalByte);
    }

    private SysInfos getSysInfo(){
        Properties props = System.getProperties();
        String osName = props.getProperty("os.name");
        String osArch = props.getProperty("os.arch");
        return  new SysInfos(osName,osArch);
    }


    private  String formatByte(long byteNumber){
        double format = 1024.0;
        double kbNumber = byteNumber/format;
        if(kbNumber<format){
            return new DecimalFormat("#.##KB").format(kbNumber);
        }
        double mbNumber = kbNumber/format;
        if(mbNumber<format){
            return new DecimalFormat("#.##MB").format(mbNumber);
        }
        double gbNumber = mbNumber/format;
        if(gbNumber<format){
            return new DecimalFormat("#.##GB").format(gbNumber);
        }
        double tbNumber = gbNumber/format;
        return new DecimalFormat("#.##TB").format(tbNumber);
    }
    public ComputerInfos getInfos(){
        return  new ComputerInfos(getSysInfo(),getMemInfo(),getCpuInfo());
    }

}
