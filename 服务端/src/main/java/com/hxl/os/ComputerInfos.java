package com.hxl.os;

import com.hxl.os.linux.Performance;

public class ComputerInfos {
    private SysInfos sysInfo;
    private MemoryInfos memoryInfos;
    private CpuInfos cpuInfos;

    public ComputerInfos(SysInfos sysInfo, MemoryInfos memoryInfos, CpuInfos cpuInfos) {
        this.sysInfo = sysInfo;
        this.memoryInfos = memoryInfos;
        this.cpuInfos = cpuInfos;
    }

    public CpuInfos getCpuInfos() {
        return cpuInfos;
    }

    public void setCpuInfos(CpuInfos cpuInfos) {
        this.cpuInfos = cpuInfos;
    }

    public SysInfos getSysInfo() {
        return sysInfo;
    }

    public void setSysInfo(SysInfos sysInfo) {
        this.sysInfo = sysInfo;
    }

    public MemoryInfos getMemoryInfos() {
        return memoryInfos;
    }

    public void setMemoryInfos(MemoryInfos memoryInfos) {
        this.memoryInfos = memoryInfos;
    }


}
