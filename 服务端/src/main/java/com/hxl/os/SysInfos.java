package com.hxl.os;

public class SysInfos {
    private String sysName;
    private String arch;

    public SysInfos(String sysName, String arch) {
        this.sysName = sysName;
        this.arch = arch;
    }

    public String getSysName() {
        return sysName;
    }

    public void setSysName(String sysName) {
        this.sysName = sysName;
    }

    public String getArch() {
        return arch;
    }

    public void setArch(String arch) {
        this.arch = arch;
    }
}
