package com.hxl.server;

import com.hxl.handlers.PerformanceHandler;
import com.sun.net.httpserver.HttpServer;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.logging.Logger;

public class MonitorHttpServer {
    Logger logger= Logger.getLogger(MonitorHttpServer.class.getName());
    private int port;
    private  HttpServer httpServer;
    public MonitorHttpServer(int port) {
        this.port = port;
        try {
            httpServer = HttpServer.create(new InetSocketAddress(port), 0);
            initContext();
            httpServer.start();
            logger.info("服务启动成功----------------->"+port+"端口");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private  void initContext(){
        httpServer.createContext("/listInfo",new PerformanceHandler());
    }

}
