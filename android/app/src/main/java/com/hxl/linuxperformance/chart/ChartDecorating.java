package com.hxl.linuxperformance.chart;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;

public class ChartDecorating {

    public ChartDecorating(Chart chart,String label) {
        Description description =new Description();
        description.setText(label);
        chart.setDescription(description);
        chart.getLegend().setEnabled(false);
        if (chart instanceof LineChart){
            LineChart lineChart = ((LineChart) chart);
            lineChart.setDrawBorders(false);

            lineChart.setDrawMarkers(false);
            lineChart.getAxisLeft().setDrawGridLines(false);
            lineChart.getAxisRight().setDrawGridLines(false);
            lineChart.setDrawBorders(false);
            lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
            lineChart.getAxisRight().setEnabled(false);
            YAxis yAxis = lineChart.getAxisLeft();
            yAxis.setTextSize(12f);
            yAxis.setAxisMinimum(0f);
            yAxis.setAxisMaximum(100f);



        }else  if (chart instanceof PieChart){
            PieChart pieChart = (PieChart) chart;
            pieChart.setDrawHoleEnabled(false);
        }

    }

}
