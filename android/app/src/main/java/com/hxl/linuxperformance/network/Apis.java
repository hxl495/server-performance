package com.hxl.linuxperformance.network;

import com.hxl.linuxperformance.entity.ComputerInfos;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Apis {

    @GET("/listInfo")
    Call<ComputerInfos> listInfos();
}
