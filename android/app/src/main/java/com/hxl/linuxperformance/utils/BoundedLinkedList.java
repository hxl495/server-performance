package com.hxl.linuxperformance.utils;

import androidx.annotation.NonNull;

import java.util.Collection;
import java.util.LinkedList;

public class BoundedLinkedList<E> extends LinkedList<E> {
    private  int size;

    public BoundedLinkedList(int size) {
        this.size = size;
    }

    public BoundedLinkedList(@NonNull Collection<? extends E> c) {
        super(c);
    }

    @Override
    public boolean offer(E e) {
        if (size()>=size){pop();}
        return super.offer(e);
    }
}
