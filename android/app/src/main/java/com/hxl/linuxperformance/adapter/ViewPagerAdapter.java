package com.hxl.linuxperformance.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.hxl.linuxperformance.ByteUtils;
import com.hxl.linuxperformance.R;
import com.hxl.linuxperformance.chart.ChartDecorating;
import com.hxl.linuxperformance.chart.DataManager;
import com.hxl.linuxperformance.entity.ComputerInfos;
import com.hxl.linuxperformance.entity.CpuInfos;
import com.hxl.linuxperformance.entity.MemoryInfos;
import com.hxl.linuxperformance.utils.BoundedLinkedList;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ViewPagerAdapter extends RecyclerView.Adapter<ViewPagerAdapter.MyViewHolder> {
    private LinkedList<CpuInfos> mCpuInfos;
    private LinkedList<MemoryInfos> mMemoryInfos;


    private Context mContext;

    public ViewPagerAdapter(Context context) {
        mContext = context;
        mCpuInfos = new BoundedLinkedList<>(10);
        mMemoryInfos = new BoundedLinkedList<>(10);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.i("TAG", "onCreateViewHolder: ");
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_line_chart, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        LineChart lineChart = holder.mLineChart;
        String label = position == 0 ? "CPU使用率图" : "内存使用率图";
        new ChartDecorating(lineChart, label);
        new ChartDecorating(holder.mPieChart, label);
        if (position == 0 && mCpuInfos.size() > 0) {
            List<Float> values = new ArrayList<>();

            for (int i = 0; i < mCpuInfos.size(); i++) {
                values.add(Float.valueOf(mCpuInfos.get(i).getSysUtilization() * 100));
            }
            lineChart.setData(DataManager.createLineData(values, "CPU使用率"));


            float value = Float.valueOf(mCpuInfos.peekLast().getSysUtilization() * 100);

            holder.mPieChart.setData(DataManager.createPieData(value));

            holder.mInfos.setText("核数:" + mCpuInfos.peekLast().getLogicalProcessorCount());


        } else if (position == 1 && mMemoryInfos.size() > 0) {

            List<Float> values = new ArrayList<>();
            for (int i = 0; i < mMemoryInfos.size(); i++) {
                values.add(Float.valueOf(String.format("%.2f", mMemoryInfos.get(i).getUtilization() * 100)));
            }

            lineChart.setData(DataManager.createLineData(values, "内存使用率"));

            float value = Float.valueOf(String.format("%.2f", mMemoryInfos.peekLast().getUtilization() * 100));
            holder.mPieChart.setData(DataManager.createPieData(value));

            holder.mInfos.setText("内存大小" + ByteUtils.formatByte(mMemoryInfos.peekLast().getTotal()));

        }

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return 2;
    }

    public void put(ComputerInfos infos) {
        mMemoryInfos.offer(infos.getMemoryInfos());
        mCpuInfos.offer(infos.getCpuInfos());
        notifyDataSetChanged();

    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        LineChart mLineChart;
        PieChart mPieChart;
        TextView mInfos;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.mLineChart = (LineChart) itemView.findViewById(R.id.linechart);
            this.mPieChart = (PieChart) itemView.findViewById(R.id.piechart);
            this.mInfos = (TextView) itemView.findViewById(R.id.info);
        }
    }
}
