package com.hxl.linuxperformance.entity;

public class CpuInfos {
    private int logicalProcessorCount;
    private float sysUtilization;

    public CpuInfos(int logicalProcessorCount, float sysUtilization) {
        this.logicalProcessorCount = logicalProcessorCount;
        this.sysUtilization = sysUtilization;
    }

    public int getLogicalProcessorCount() {
        return logicalProcessorCount;
    }

    public void setLogicalProcessorCount(int logicalProcessorCount) {
        this.logicalProcessorCount = logicalProcessorCount;
    }

    public float getSysUtilization() {
        return sysUtilization;
    }

    public void setSysUtilization(float sysUtilization) {
        this.sysUtilization = sysUtilization;
    }
}
