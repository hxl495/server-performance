package com.hxl.linuxperformance.entity;


public class MemoryInfos {
    private  long  total;
    private long  userTotal;

    private double utilization;

    public MemoryInfos(long total, long userTotal, double utilization) {
        this.total = total;
        this.userTotal = userTotal;
        this.utilization = utilization;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getUserTotal() {
        return userTotal;
    }

    public void setUserTotal(long userTotal) {
        this.userTotal = userTotal;
    }

    public double getUtilization() {
        return utilization;
    }

    public void setUtilization(double utilization) {
        this.utilization = utilization;
    }
}
