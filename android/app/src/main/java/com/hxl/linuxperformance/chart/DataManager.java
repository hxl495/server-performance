package com.hxl.linuxperformance.chart;

import android.graphics.Color;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.List;

public class DataManager {
    public static LineData createLineData(List<Float> values,String label){
        List<Entry> list = new ArrayList<>();
        for (int i = 0; i < values.size(); i++) {
            list.add(new Entry(i,values.get(i)));
        }
        LineDataSet set = new LineDataSet(list, label);

        set.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return String.format("%.2f",value);
            }
        });
        LineData lineData = new LineData(set);
        return lineData;
    }
    public static PieData createPieData(float  value){

        List<PieEntry> pieEntries =new ArrayList<>();
        pieEntries.add(new PieEntry(value,"已用"));

        pieEntries.add(new PieEntry(100f-value,"剩余"));
        PieDataSet pieDataSet = new PieDataSet(pieEntries,"");
        if (value>90){
            pieDataSet.setColors(Color.parseColor("#03a9f4"),Color.parseColor("#f44336"));
        }else {
            pieDataSet.setColors(Color.parseColor("#03a9f4"),Color.parseColor("#4caf50"));
        }
        PieData pieData = new PieData(pieDataSet);
        return pieData;
    }
}
